# IPSC Agent - CTI Server network libraries documentation

## How to read

Read the markdown files directly, or build them into a web site, then read by a browser.

## How to build

Install Python 2.7 or latest Python3.x, including `pip`.

And then

```sh
cd /path/to/the/project
pip install --user -r requirements.txt
mkdocs build
```

The built directory is `site`.

If want a live preview, execute:

```
mkdocs serve
```