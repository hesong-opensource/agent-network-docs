# API

## 常量

### WorkType

**工作类型 :**

Value | Description
----- | ----------------
0     | 未使用
1     | 呼入
2     | 转接
3     | 手动电脑呼出
4     | 手动电话键盘呼出
5     | 咨询
6     | 执行流程
7     | 强插
8     | 监听
9     | 保持
10    | 忙碌
11    | 离开
12    | 打字
13    | 保留类型1
14    | 保留类型2
15    | 保留类型3
16    | 保留类型4
17    | 保留类型5

### TeleMode

**话机模式 :**

Value | Description
----- | ---------------
`0`   | 硬电话,手柄模式
`1`   | 软电话,耳机模式
`2`   | 外部模式
`3`   | IP模式

### SubFlowCallType

**调用模式:**

Value | Description
----- | ------------------------------------------------------------------------
`0`   | 保持会话模式: 座席当前所在的会话也会并入子流程引发的会话，当前会话将继续
`1`   | 结束会话模式: 座席当前所在的会话不会并入子流程,当前会话将中断

### TeleState

**话机状态:**

Value | Description
----- | -----------
`0`   | 挂机
`1`   | 摘机

### QueueType

**排队类型:**

Value | Description
----- | -----------
`0`   | 呼入
`1`   | 转接
`2`   | 咨询
`3`   | 重新返回
`4`   | 外拨转入
`5`   | 拦截
`6`   | 内部呼叫

### QueueInfoType

排队情况发生变化的类型

Value | Description
----- | --------------
0     | 排队加入
1     | 排队等座席
2     | 排队被接听
3     | 排队超时
4     | 排队取消(删除)

## 回调

### 服务器消息回调

```c
typedef long (stdcall *PCALLBACK_FUNC)(long msg_type,long param1,long param2, const char * pcParam);
```

定义这个类型的函数，用于接收来自服务器的消息。

## 函数

### 初始化

```c
bool stdcall Initialize();
```

初始化资源，在使用其它函数前必须调用

**Return :**
初始化是否成功

### 释放

```c
void stdcall Finalize();
```

释放资源，程序退出时务必调用，以便“优雅的”关闭。

### 设置服务器消息回调函数

```c
long stdcall RegCallBackFunc(PCALLBACK_FUNC CallBackAddress);
```

**Parameters :**

Parameters        | Description
----------------- | -----------------------------------------------
`CallBackAddress` | 回调函数。见  [PCALLBACK_FUNC](#服务器消息回调)

### 建立与服务器之间连接

```c
int stdcall ConnectServer(const char *pName, unsigned int nPort, const char *option);
```

**Parameters :**

Parameters | Description
---------- | --------------
`pName`    | 服务器的IP地址
`nPort`    | 服务器的端口
`option`   | 连接选项

**Return :**

- `0`: 执行失败
- `1`: 执行成功（不是连接成功！）

<!-- TODO:  options 说明-->

!!! important
    一旦建立连接，在调用 <#断开连接> 之前，如果出现连接中断，这个库不会自动重联。

### 断开与服务器之间连接

```c
void stdcall DisconnectServer(void);
```

### 向服务器发送消息

```c
int  stdcall NotifyServer(int commandType, int nParam, const char* pcParam);
```

**Parameters :**

Parameters    | Description
------------- | ----------------
`commandType` | 消息的命令类型
`nParam`      | 消息的整数部分
`pcParam`     | 消息的字符串部分

**Return :**

- `0`: 执行失败
- `1`: 执行成功（并不能确定服务器已经收到消息！）
