# 客户端-服务器消息

## 概述

客户端-服务器消息的发送，是通过 [NotifyServer](../api.md#向服务器发送消息) 函数实现的。

客户端通过该函数的三个参数 `int commandType`, `int nParam`, `const char *pcParam` 组合成不同的消息发送给服务器。

本章节用于说明这三个参数如何组合成服务器可以识别的命令。

!!! note
    如遇未予说明的情况，`nParam`参数的值应默认为`0`，`pcParam`参数的值应默认为`""`(空字符串)

## 请求消息格式

请求消息是由座席客户端发送给CTI服务器的，座席发送的请求消息的各种命令格式定义如下：

### 登录

`commandType`
:   `REMOTE_MSG_LOGIN`

`pcParam`
:   格式: `<登录工号>|<登录密码>|<验证类型>|<话机模式>`

### 注销

`commandType`
:   `REMOTE_MSG_RELEASE`

### 签入

`commandType`
:   `REMOTE_MSG_SIGNON`

`pcParam`
:   要签入的座席组ID。
    如果是空字符串,则签入所有座席组。
    如果需要一次性签入多个组,可在字符串中填写多个组ID，用“|”符号分隔。

### 签出

`commandType`
:   `REMOTE_MSG_SIGNOFF`

`pcParam`
:   要签出的座席组ID。
    如果是空字符串,则签出所有座席组。
    如果需要一次性签出多个组,可在字符串中填写多个组ID，用“|”符号分隔。

### 示忙

`commandType`
:   `REMOTE_MSG_PAUSE`

`nParam`
:   示忙类型，见[工作类型](../api.md#worktype)

`pcParam`
:   要签出的座席组ID。
    如果是空字符串,则签出所有座席组。
    如果需要一次性签出多个组,可在字符串中填写多个组ID，用“|”符号分隔。

!!! note
    `nParam` 的值**必须**在工作类型 `[10, 15]` 区间内。

### 示闲

`commandType`
:   `REMOTE_MSG_CONTINUE`

### 抢接

抢接另外一个座席的电话

`commandType`
:   `REMOTE_MSG_INTERCEPT`

`nParam`
:   要抢接的目标座席的ID

### 外呼

拨叫外线电话

`commandType`
:   `REMOTE_MSG_DIAL`

`pcParam`
:   拨号参数，格式是 `<CalledTeleNum>|<CallingTeleNum>|<LinkNo>|[CallChannel]|[ChannelGroup]|[CustomString]`

**拨号参数说明:**

Arguments      | Description
-------------- | -----------------------------------------------------------
CalledTeleNum  | 被叫电话号码。如果为空,则需要在座席话机上手动按键输入号码。
CallingTeleNum | 主叫号码。空表示不特别指定。
LinkNo         | 外呼所使用的链路的号码。-1表示系统自动分配。
CallChannel    | 呼叫通道。不填表示系统自动分配
ChannelGroup   | 呼叫所使用的通道组。空字符串表示不特别指定通道组。
CustomString   | 发起呼叫动作时,传递给服务器的自定义字符串参数。

### 呼转到坐席

将通话转移到其它座席。

`commandType`
:   `REMOTE_MSG_TRANSFER`

`nParam`
:   目标座席ID

`pcParam`
:   转移参数，格式是 `<Channel>|<AgentGroup>|[CustomString]`

**转移参数定义 :**

Arguments    | Description
------------ | ----------------------------------------------
Channel      | 待转移的通道号
AgentGroup   | 目标座席所在的座席组的ID
CustomString | 发起转移动作时，传递给服务器的自定义字符串参数

### 呼转到外线

将通话转移到外线。

`commandType`
:   `REMOTE_MSG_TRANSFER_EX`

`nParam`
:   待转移的通道。`-1`表示当前坐席正在通话的通道。

`pcParam`
:   转移参数，格式是 `<CalledTeleNum>|<CallingTeleNum>|<LinkNo>|[CallChannel]|[ChannelGroup]|[CustomString]`

Arguments      | Description
-------------- | -----------------------------------------------------------
CalledTeleNum  | 被叫电话号码。如果为空,则需要在座席话机上手动按键输入号码。
CallingTeleNum | 主叫号码。空表示不特别指定。
LinkNo         | 外呼所使用的链路的号码。-1表示系统自动分配。
CallChannel    | 呼叫通道。不填表示系统自动分配
ChannelGroup   | 呼叫所使用的通道组。空字符串表示不特别指定通道组。
CustomString   | 发起呼叫动作时,传递给服务器的自定义字符串参数。

### 咨询到座席

咨询到另外一个座席。调用该方法后，本机座席的当前通话将被放到通话保持队列，同时服务器呼叫被咨询的目标座席。此时，一旦被咨询的目标座席接听电话之后，再调用“找回”方法（Retrieve），即可在目标座席、本座席以及原主叫方之间进行三方通话。

`commandType`
:   `REMOTE_MSG_CONSULT`

`nParam`
:   目标座席ID

`pcParam`
:   字符参数，格式是 `<Channel>|<AgentGroup>|[CustomString]`

**字符参数定义 :**

Arguments    | Description
------------ | ----------------------------------------------
Channel      | 待咨询的通道号
AgentGroup   | 目标座席所在的座席组的ID
CustomString | 发起咨询动作时，传递给服务器的自定义字符串参数

### 咨询到外线

咨询到外线电话。调用该方法后，本机座席的当前通话将被放到通话保持队列，同时服务器呼叫被咨询的目标外线。此时，一旦被咨询的目标外线接听电话之后，再调用“找回”方法（Retrieve），即可在目标外线、本座席以及原主叫方之间进行三方通话。

`commandType`
:   `REMOTE_MSG_CONSULT_EX`

`pcParam`
:   转移参数，格式是 `<CalledTeleNum>|<CallingTeleNum>|<LinkNo>|[CallChannel]|[ChannelGroup]|[CustomString]`

**字符参数说明 :**

Arguments      | Description
-------------- | -----------------------------------------------------------
CalledTeleNum  | 被叫电话号码。如果为空,则需要在座席话机上手动按键输入号码。
CallingTeleNum | 主叫号码。空表示不特别指定。
LinkNo         | 外呼所使用的链路的号码。-1表示系统自动分配。
CallChannel    | 呼叫通道。不填表示系统自动分配
ChannelGroup   | 呼叫所使用的通道组。空字符串表示不特别指定通道组。
CustomString   | 发起呼叫动作时,传递给服务器的自定义字符串参数。

### 通话保持

通话保持。保持当前通话，使得该会话进入保持队列。

`commandType`
:   `REMOTE_MSG_HOLD`

### 取消保持

找回被保持的通话。从保持队列中将指定通道所对应的会话找回，重新与之通话。    

`commandType`
:   `REMOTE_MSG_RETRIEVE`

`nParam`
:   目标通道

### 切线

切断与一个外线通道的会话，即使正在保持的会话也会被挂断。同时传递参数到服务所使用的流程。

`commandType`
:   `REMOTE_MSG_BREAK_SESS`

`nParam`
:   目标通道。-1表示挂断当前通道

`pcParam`
:   格式：`<strParamsToFlow>`

**字符参数说明 :**

Arguments    | Description
------------ | --------------------------
ParamsToFlow | 传递给服务所使用流程的参数

### 挂机

座席挂机。仅当话机模式为耳机模式时有效。

`commandType`
:   `REMOTE_MSG_HANGUP`

### 摘机

座席摘机。仅当话机模式为耳机模式时有效。

`commandType`
:   `REMOTE_MSG_OFFHOOK`

### 强插

`commandType`
:   `REMOTE_MSG_FORCEHANGUP`

`nParam`
:   目标座席 ID

### 开始监听

对其它座席进行监听。

`commandType`
:   `REMOTE_MSG_LISTEN`

`nParam`
:   目标座席 ID

### 停止监听

停止对其它座席进行监听。

`commandType`
:   `REMOTE_MSG_STOPLISTEN`

`nParam`
:   目标座席 ID

### 取排队

与拦截类似,可以抢接到其它通道上的会话。

`commandType`
:   `REMOTE_MSG_GETQUEUE`

`nParam`
:   目标通道号

### 设置闭塞

闭塞其它座席.

`commandType`
:   `REMOTE_MSG_BLOCK`

`nParam`
:   目标座席 ID

### 解除闭塞

解除对其它座席的闭塞

`commandType`
:   `REMOTE_MSG_UNBLOCK`

`nParam`
:   目标座席 ID

### 踢出

强行注销其它座席。

`commandType`
:   `REMOTE_MSG_KICKOUT`

`nParam`
:   目标座席 ID

### 强行签出

强行将其它座席从座席组中签出。

`commandType`
:   `REMOTE_MSG_FORCESIGNOFF`

`nParam`
:   目标座席 ID

`pcParam`
:   座席要签出的座席组

### 切换话机模式

对集成内部 VoIP 软电话的座席客户端无效。

`commandType`
:   `REMOTE_MSG_TELEMODE`

`nParam`
:   话机模式。见[TeleMode](../api.md#TeleMode)

### 调用IVR

调用服务器上的子流程。

`commandType`
:   `REMOTE_MSG_CALLSUBFLOW`

`nParam`
:   IVR的启动通道

`pcParam`
:   字符串参数，格式：`<SubFlowName>|<CallType>|<ParamString>`

**字符串参数说明:**

Argument    | Description
----------- | -------------------------
SubFlowName | 要启动的IVR流程的子项目名
CallType    | 调用模式，见[SubFlowCallType](../api.md#subflowcalltype)

## 请求回复模型

座席客户端通过[`服务器消息回调`](../api.md#服务器消息回调)收取三种消息：

1. 连接建立/断开的通知消息，它是由这个网络通信库自行抛出的
1. 来自CTI服务器的主动消息通知，如来电振铃通知
1. 来自CTI服务器的被动消息回复，用于告知客户端其请求命令的执行响应情况

所以，座席客户端在使用这个通信库发送请求命令后，要等待来自CTI服务器的被动消息回复，确定其请求命令的执行响应情况。

在这种情况下

```c
typedef long (stdcall *PCALLBACK_FUNC)(long msg_type,long param1,long param2, const char * pcParam);
```

回调函数的 `long msg_type` 参数用于和请求消息配对，其工作步骤是：

1. 座席调用`NotifyServer`发送某个命令，其`commandType`为命令的ID
1. 服务器收到后，如果可以开始执行该命令，就像座席发送一条回复消息，回复消息的`msg_type`与收到的命令ID一致
1. 座席通过`PCALLBACK_FUNC`回调等待这个回复，如果长时间等不到，就认为服务器响应超时

## 登录

同步执行？？？

## 注销

同步执行？？？
