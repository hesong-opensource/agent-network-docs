# 服务器-客户端消息

服务器-客户端消息的接收，是通过 [PCALLBACK_FUNC](../api.md#服务器消息回调) 回调实现的。
座席客户端通过它收取三种消息：

1. 来自CTI服务器的被动消息回复，用于告知客户端其请求命令的执行响应情况
1. 来自CTI服务器的主动事件消息通知，如来电振铃通知
1. 连接建立/断开的通知消息，它是由这个网络通信库自行抛出的

## 回复

座席客户端在使用这个通信库发送请求命令后，要等待来自CTI服务器的被动消息回复，确定其请求命令的执行响应情况。

在这种情况下

```c
typedef long (stdcall *PCALLBACK_FUNC)(long msg_type,long param1,long param2, const char * pcParam);
```

回调函数的 `long msg_type` 参数用于和请求消息配对，其工作步骤是：

1. 座席调用`NotifyServer`发送某个命令，其`commandType`为命令的ID
1. 服务器收到后，如果可以开始执行该命令，就像座席发送一条回复消息，回复消息的`msg_type`与收到的命令ID一致
1. 座席通过`PCALLBACK_FUNC`回调等待这个回复，如果长时间等不到，就认为服务器响应超时

## 事件


### 座席状态改变

`msg_type`
:   `REMOTE_MSG_SETSTATE`

`param1`
:   变化后的座席状态

`param2`
:   变化后的工作状态

### 话机状态改变

`msg_type`
:   `REMOTE_MSG_SETTELESTATE`

`param1`
:   变化后的话机状态. 见 [TeleState](../api.md#telestate)

### 话机模式改变

`msg_type`
:   `REMOTE_MSG_SETTELEMODE`

`param1`
:   变化后的话机模式. 见 [TeleMode](../api.md#telemode)

### 排队信息

座席获得排队信息时触发。当座席所在座席组的排队列表发生变化的时候会引发该事件。

`msg_type`
:   `REMOTE_MSG_QUEUEINFO`

`param1`
:   排队情况发生变化的外线通道序号

`param2`
:   排队情况发生变化的类型。见[QueueInfoType](../api.md#queueinfotype)

`pcParam`
:   字符参数，格式：`[AgentGroup]|<QueueType>|<SessionId><QueueId>|<AgentId>|[CallingTeleNum]|[CustomString]`

**字符参数:**

Argument       | Description
-------------- | ---------------------------------------------------------
AgentGroup     | 排队情况发生变化的座席组的 ID
QueueType      | 排队类型,有呼入、转接、咨询、重新返回、外拨转入这几种类型.见[QueueType](../api.md#queuetype)
SessionId      | 会话 ID
QueueId        | 排队 ID
AgentId        | 座席 ID
CallingTeleNum | 排队电话的主叫号码
CustomString   | 自定义信息

### 保持信息

当座席的通话保持队列发生变化时触发。当通话被保持或者从保持状态恢复都会引发该
事件。

`msg_type`
:   `REMOTE_MSG_HOLDINFO`

`param1`
:   队列中发生变化的通话的外线通道序号

`param2`
:   保持的类型。见[QueueInfoType](../api.md#queueinfotype)

`pcParam`
:   字符参数，格式：`<会话 ID>`

**字符参数:**

Argument       | Description
-------------- | ---------------------------------------------------------
AgentGroup     | 排队情况发生变化的座席组的 ID
QueueType      | 排队类型,有呼入、转接、咨询、重新返回、外拨转入这几种类型.见[QueueType](../api.md#queuetype)
SessionId      | 会话 ID
QueueId        | 排队 ID
AgentId        | 座席 ID
CallingTeleNum | 排队电话的主叫号码
CustomString   | 自定义信息

### 坐席振铃

当座席响铃的时候触发

`msg_type`
:   `REMOTE_MSG_SENDDATA`

`param1`
:   `DATATYPE_RING_INFO`

`param2`
:   响铃时等待接通的外线通道序号

`pcParam`
:   响铃时由服务器传送来的自定义字符串。它通常是由“|”字符分割的串。

### 工作通道变化

工作通道（与内线接通的外线通道）变化时触发。它将会导致WorkChannel属性的变化。

`msg_type`
:   `REMOTE_MSG_SENDDATA`

`param1`
:   `DATATYPE_WORK_CHNO`

`pcParam`
:   变化发生时的会话ID

### 座席组变化

座席所属的座席组发生变化时触发。

`msg_type`

:   `REMOTE_MSG_SENDDATA`

`param1`
:   `DATATYPE_SIGNON_GROUP`

`pcParam`
:   变化发生时的会话ID

### 流程消息

收到来自流程的消息时触发。

`msg_type`
:   `REMOTE_MSG_SENDDATA`

`param1`
:   `DATATYPE_IVR_SEND_DATA`

`param2`
:   自定义信息中的整型参数

`pcParam`
:   自定义信息中的一个字符型形参数

### 获得座席ID

当获得座席的ID后触发。通常在成功登录后发生。

`msg_type`
:   `REMOTE_MSG_SENDDATA`

`param1`
:   `DATATYPE_AGENTID`

### 获得内线通道

当获得座席的所对应得到内线通道序号后触发。通常在成功登录后发生

`msg_type`
:   `REMOTE_MSG_SENDDATA`

`param1`
:   `DATATYPE_CHNO`

### 话机模式变化

在座席的话机模式发生变化后触发。一般来说，当座席通知服务器修改话机模式后，如果服务器响应并且允许了话机模式的变化，该事件将被触发。

`msg_type`
:   `REMOTE_MSG_SENDDATA`

`param1`
:   `DATATYPE_TELEMODE`

`param2`
:   事件发生之后的[话机模式](../api.md#telemode)值

## 连接

### 连接服务器成功

在座席连接服务器成功后触发。

`msg_type`
:   `GET_MSG_TYPE_CONNECT`

### 连接服务器失败

在座席连接服务器失败后触发。

`msg_type`
:   `GET_MSG_TYPE_CONNECT_FAIL`

`param`
:   失败原因编码

**失败原因 :**

Value | Description
----- | --------------------------------
0     | 无法连接到主机
1     | 已经连接到了主机
2     | 主机禁止连接
3     | 主机无资源
4     | 连接密码错误
5     | 没有找到对应该计算机名的 AgentId
6     | 该 AgentId 已经登录
其它  | 其它未知错误

### 断开连接

在服务器主动断开座席连接后触发

`msg_type`
:   `GET_MSG_TYPE_DISCONNECT`

### 连接丢失

与 VCP/IPSC 服务器之间的连接异常丢失的时候被触发。

`msg_type`
:   `GET_MSG_TYPE_LOSTCONNECT`
